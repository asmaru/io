A simple PHP filesystem in a single class.

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/7ba89031507c48b2aa831cf2b8adcdb1)](https://www.codacy.com?utm_source=git@bitbucket.org&amp;utm_medium=referral&amp;utm_content=asmaru/io&amp;utm_campaign=Badge_Grade)

## Usage ##

```
#!php
<?php
$fs = new FileSystem(__DIR__);
$fs->write('file.txt', 'This is an example');
echo $fs->read('file.txt');
$fs->delete('file.txt');
```