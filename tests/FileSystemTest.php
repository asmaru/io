<?php

declare(strict_types=1);

namespace asmaru\io;

use Exception;
use FilesystemIterator;
use PHPUnit\Framework\TestCase;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;
use function basename;
use function file_put_contents;
use function uniqid;
use const DIRECTORY_SEPARATOR;

class FileSystemTest extends TestCase {

	private string $testDir;

	public function testConstruct() {
		try {
			new FileSystem($this->testDir);
		} catch (Exception $e) {
			$this->fail();
		}
		$this->assertTrue(true);
	}

	public function testThrowsExceptionWithInvalidPath() {
		$this->expectException(IOException::class);
		new FileSystem('invalid');
	}

	public function testName() {
		$fileSystem = new FileSystem($this->testDir);
		$this->assertEquals(basename($this->testDir), $fileSystem->name());
	}

	public function testExist() {
		$key = md5(uniqid('', true));
		$data = md5(uniqid('', true));
		$fileSystem = new FileSystem($this->testDir);
		$this->assertFalse($fileSystem->isFile($key));
		$fileSystem->write($key, $data);
		$this->assertTrue($fileSystem->isFile($key));
	}

	public function testDirExists() {
		$key = md5(uniqid('', true));
		$fileSystem = new FileSystem($this->testDir);
		$fileSystem->createDir($key);
		$this->assertTrue($fileSystem->isDir($key));
	}

	public function testWriteReadDelete() {
		$key = md5(uniqid('', true));
		$data = md5(uniqid('', true));
		$fileSystem = new FileSystem($this->testDir);
		$fileSystem->write($key, $data);
		$this->assertEquals($data, $fileSystem->read($key));
	}

	public function testReadForNotExistingFile() {
		$fileSystem = new FileSystem($this->testDir);
		$this->assertNull($fileSystem->read(''));
	}

	public function testCanDeleteAll() {
		$fileSystem = new FileSystem($this->testDir);
		$fileSystem->write('1', '1');
		$fileSystem->write('2', '2');
		$fileSystem->deleteAll();
		$this->assertEmpty($fileSystem->files());
	}

	public function testAppend() {
		$key = md5(uniqid('', true));
		$fileSystem = new FileSystem($this->testDir);
		$fileSystem->write($key, '1');
		$fileSystem->append($key, '2');
		$fileSystem->append($key, '3');
		$this->assertEquals('123', $fileSystem->read($key));
	}

	public function testGet() {
		$key = md5(uniqid('', true));
		$data = md5(uniqid('', true));
		$fileSystem = new FileSystem($this->testDir);
		$fileSystem->write($key, $data);
		$splFileInfo = $fileSystem->get($key);
		$this->assertNotNull($splFileInfo);
	}

	public function testGetWithInvalidKey() {
		$fileSystem = new FileSystem($this->testDir);
		$this->assertNull($fileSystem->get('invalid'));
	}

	public function testCanGetFiles() {
		$fileSystem = new FileSystem($this->testDir);

		$fileSystem->write('file1.aaa', '');
		$fileSystem->write('file2.bbb', '');
		$fileSystem->write('file3.ccc', '');

		// all files
		$files = $fileSystem->files();
		$this->assertCount(3, $files);

		// with extension filter
		$files = $fileSystem->files(['aaa', 'bbb']);
		$this->assertCount(2, $files);
	}

	public function testReadAll() {
		$fileSystem = new FileSystem($this->testDir);

		$fileSystem->write('file1.aaa', 'aaa');
		$fileSystem->write('file2.bbb', 'bbb');
		$fileSystem->write('file3.ccc', 'ccc');

		$files = $fileSystem->readAll();
		$this->assertCount(3, $files);
		$this->assertEquals('aaa', $files['file1.aaa']);
		$this->assertEquals('bbb', $files['file2.bbb']);
		$this->assertEquals('ccc', $files['file3.ccc']);

		$files = $fileSystem->readAll([], true);
		$this->assertCount(3, $files);
		$this->assertEquals('aaa', $files['file1']);
		$this->assertEquals('bbb', $files['file2']);
		$this->assertEquals('ccc', $files['file3']);

		$files = $fileSystem->readAll(['bbb']);
		$this->assertCount(1, $files);
		$this->assertEquals('bbb', $files['file2.bbb']);
	}

	public function testCreate() {
		$fileSystem = new FileSystem($this->testDir);

		// with name
		$this->assertNotNull($fileSystem->create('file1.aaa'));
		$this->assertTrue($fileSystem->isFile('file1.aaa'));

		// with random name
		$file = $fileSystem->create();
		$this->assertNotNull($file);
		$this->assertTrue($fileSystem->isFile($file->getBasename()));
	}

	public function testSecurity() {
		$fileSystem = new FileSystem($this->testDir);

		// path traversal is not allowed
		$file = $fileSystem->create('../aaa.bbb');
		$this->assertEquals('aaa.bbb', $file->getBasename());
	}

	public function testCanImport() {
		$dirname = md5(uniqid('', true));
		$testFile = $this->testDir . DIRECTORY_SEPARATOR . $dirname . DIRECTORY_SEPARATOR . 'test.txt';
		mkdir($this->testDir . DIRECTORY_SEPARATOR . $dirname);
		file_put_contents($testFile, 'test');
		$fileSystem = new FileSystem($this->testDir);
		$this->assertTrue($fileSystem->import($testFile) instanceof SplFileInfo);
		$this->expectException(IOException::class);
		$fileSystem->import($testFile);
	}

	public function testCanGetDirs() {
		$fileSystem = new FileSystem($this->testDir);
		$fileSystem->createDir('1');
		$fileSystem->createDir('2');
		$fileSystem->createDir('3');
		$result = $fileSystem->dirs();
		$this->assertCount(3, $result);
		$this->assertArrayHasKey('1', $result);
		$this->assertArrayHasKey('2', $result);
		$this->assertArrayHasKey('3', $result);
	}

	public function testCanGoDownAndUp() {
		$fileSystem = new FileSystem($this->testDir);
		$fileSystem->createDir('a');
		$a = $fileSystem->down('a');
		$this->assertEquals('a', $a->name());
		$this->assertEquals($fileSystem->path(), $a->up()->path());
	}

	public function setUp(): void {
		$this->testDir = __DIR__ . DIRECTORY_SEPARATOR . md5(uniqid('', true));
		mkdir($this->testDir);
	}

	public function tearDown(): void {
		$path = $this->testDir;
		if (is_file($path)) {
			unlink($path);
		} else {
			$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST);
			foreach ($iterator as $filename => $fileInfo) {
				if ($fileInfo->isDir()) {
					rmdir($filename);
				} else {
					unlink($filename);
				}
			}
			rmdir($path);
		}
	}
}