<?php

namespace asmaru\io;

use FilesystemIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;
use ZipArchive;
use function is_dir;
use function sprintf;

/**
 * Class FileUtility
 *
 * @package asmaru\io
 */
class FileUtility {

	/**
	 * @param string $source
	 * @param string $target
	 *
	 * @throws IOException
	 */
	public function copyRecursive(string $source, string $target) {
		if (!is_dir($source)) {
			throw new IOException(sprintf('Source not found in "%s"', $source));
		}
		if (!is_dir($target)) {
			mkdir($target, 0755, true);
		}
		/** @var RecursiveDirectoryIterator $iterator */
		$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::SELF_FIRST);
		foreach ($iterator as $item) {
			if ($item->isDir()) {
				mkdir($target . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
			} else {
				copy($item, $target . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
			}
		}
	}

	/**
	 * @param string $source
	 * @param string $target
	 */
	public function createZIPFromDir(string $source, string $target) {
		$zip = new ZipArchive();
		$zip->open($target, ZipArchive::CREATE | ZipArchive::OVERWRITE);

		/** @var SplFileInfo[] $files */
		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::LEAVES_ONLY);
		foreach ($files as $name => $file) {
			// Skip directories (they would be added automatically)
			if (!$file->isDir()) {

				// Get real and relative path for current file
				$filePath = $file->getRealPath();
				$relativePath = substr($filePath, strlen($source));

				// Add current file to archive
				$zip->addFile($filePath, $relativePath);
			}
		}
		$zip->close();
	}

	/**
	 * @param string $path
	 * @param bool $includeSelf
	 */
	public function deleteRecursive(string $path, bool $includeSelf = true) {
		if (is_file($path)) {
			unlink($path);
		} else {
			$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST);
			foreach ($iterator as $filename => $fileInfo) {
				if ($fileInfo->isDir()) {
					rmdir($filename);
				} else {
					unlink($filename);
				}
			}
			if ($includeSelf) {
				rmdir($path);
			}
		}
	}
}