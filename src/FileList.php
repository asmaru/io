<?php

declare(strict_types=1);

namespace asmaru\io;

use ArrayAccess;
use Countable;
use Iterator;
use SplFileInfo;

class FileList implements ArrayAccess, Countable, Iterator {

	/**
	 * @var array|SplFileInfo[]
	 */
	private array $files = [];

	private int $position = 0;

	public function add(SplFileInfo $file): void {
		$this->files[] = $file;
	}

	public function count(): int {
		return count($this->files);
	}

	public function current(): mixed {
		return $this->files[$this->position];
	}

	public function key(): int {
		return $this->position;
	}

	public function next(): void {
		++$this->position;
	}

	public function offsetExists(mixed $offset): bool {
		return isset($this->files[$offset]);
	}

	public function offsetGet(mixed $offset): ?SplFileInfo {
		return $this->files[$offset] ?? null;
	}

	public function offsetSet(mixed $offset, mixed $value): void {
		if (is_null($offset)) {
			$this->files[] = $value;
		} else {
			$this->files[$offset] = $value;
		}
	}

	public function offsetUnset(mixed $offset): void {
		unset($this->files[$offset]);
	}

	public function rewind(): void {
		$this->position = 0;
	}

	public function valid(): bool {
		return isset($this->files[$this->position]);
	}
}