<?php

declare(strict_types=1);

namespace asmaru\io;

use DirectoryIterator;
use FilesystemIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;
use function array_map;
use function dirname;
use function file_get_contents;
use function in_array;
use function is_dir;
use function is_file;
use function mkdir;
use function sprintf;
use function str_replace;
use const DIRECTORY_SEPARATOR;
use const false;

/**
 * Class FileSystem
 *
 * A simple filesystem for a single directory.
 *
 * @package asmaru\io
 */
class FileSystem {

	/**
	 * @var string The base path for this directory
	 */
	private string $base;

	/**
	 * FileSystem constructor.
	 *
	 * @param string $base1
	 *
	 * @throws IOException
	 */
	public function __construct(string $base1) {
		$base = realpath($base1);
		if ($base === false || !is_dir($base) || !is_writable($base)) {
			throw new IOException(sprintf('the base path "%s" is no directory or not writable', $base1));
		}
		$this->base = $base;
	}

	/**
	 * Append the given content to the file
	 *
	 * @param string $key
	 * @param string $data
	 *
	 * @return bool
	 */
	public function append(string $key, string $data): bool {
		return file_put_contents($this->path($key), $data, LOCK_EX | FILE_APPEND) !== false;
	}

	/**
	 * @param bool $includeChildren
	 *
	 * @return int
	 * @throws IOException
	 */
	public function count(bool $includeChildren = false): int {
		$count = 0;
		foreach ($this->all() as $fileInfo) {
			if ($fileInfo->isFile()) $count++;
			if ($includeChildren && $fileInfo->isDir()) {
				$count += $this->down($fileInfo->getRealPath())->count($includeChildren);
			}
		}
		return $count;
	}

	/**
	 * Create a empty file with the given name. If no name is specified, a random name will be used.
	 *
	 * @param string|null $key
	 *
	 * @return null|SplFileInfo The created file
	 */
	public function create(string $key = null): SplFileInfo {
		// create a unique random key
		if (empty($key)) {
			do {
				$key = substr(md5(uniqid('', true)), 0, 16);
			} while ($this->isFile($key));
		}
		touch($this->path($key));
		return $this->get($key);
	}

	/**
	 * Get the File or null if the file does not exist
	 *
	 * @param string $key
	 *
	 * @return null|SplFileInfo
	 */
	public function get(string $key): ?SplFileInfo {
		if ($this->isFile($key) || $this->isDir($key)) {
			return new SplFileInfo($this->path($key));
		}
		return null;
	}

	/**
	 * @param string $key
	 *
	 * @return boolean
	 */
	public function isDir(string $key): bool {
		return is_dir($this->path($key));
	}

	/**
	 * @param string $key
	 *
	 * @return FileSystem
	 * @throws IOException
	 */
	public function createDir(string $key): FileSystem {
		if ($this->isDir($key)) return $this->down($key);
		$path = $this->path($key);
		mkdir($path);
		return $this->down($key);
	}

	/**
	 * Descend to a child directory.Returns a new filesystem.
	 *
	 * @param string $key
	 *
	 * @return FileSystem
	 * @throws IOException
	 */
	public function down(string $key): FileSystem {
		return new static($this->path($key));
	}

	/**
	 * Delete the file or directory recursive
	 *
	 * @param string $key
	 *
	 * @return void
	 */
	public function delete(string $key): void {
		(new  FileUtility())->deleteRecursive($this->path($key));
	}

	/**
	 * Delete all files in this filesystem
	 *
	 * @throws IOException
	 */
	public function deleteAll() {
		$iterator = new DirectoryIterator($this->base);
		foreach ($iterator as $fileInfo) {
			if (!$fileInfo->isDot() && $fileInfo->isFile()) {
				if (!unlink($fileInfo->getRealPath())) {
					throw new IOException(sprintf('failed to delete file "%s"', $fileInfo->getRealPath()));
				}
			}
		}
	}

	/**
	 * Get all dirs in this filesystem as array. The name will be used as key.
	 *
	 * @return array
	 * @throws IOException
	 */
	public function dirs(): array {
		$dirs = [];
		$iterator = new DirectoryIterator($this->base);
		foreach ($iterator as $fileInfo) {
			if (!$fileInfo->isDot() && $fileInfo->isDir()) {
				$dirs[$fileInfo->getBasename()] = new static($fileInfo->getRealPath());
			}
		}
		return $dirs;
	}

	/**
	 * Get all files in this filesystem as array. The filename will be used as key.
	 *
	 * @param array $extensions A list of extensions to filter from the result
	 *
	 * @return FileList
	 */
	public function files(array $extensions = []): FileList {
		$extensions = array_map(function ($extension) {
			return mb_strtolower($extension);
		}, $extensions);
		$files = new FileList();
		$iterator = new DirectoryIterator($this->base);
		foreach ($iterator as $fileInfo) {
			if (!$fileInfo->isDot() && $fileInfo->isFile()) {
				$extension = mb_strtolower($fileInfo->getExtension());
				if (empty($extensions) || in_array($extension, $extensions)) {
					$files->add(new SplFileInfo($fileInfo->getRealPath()));
				}
			}
		}
		return $files;
	}

	public function getSelf(): SplFileInfo {
		return new SplFileInfo($this->base);
	}

	/**
	 * Copy a file into the filesystem
	 *
	 * @param string $source
	 * @param string|null $newName
	 *
	 * @return SplFileInfo
	 * @throws IOException
	 */
	public function import(string $source, ?string $newName = null): SplFileInfo {
		$key = basename($source);
		if ($this->isFile($key)) {
			throw new IOException(sprintf('file "%s" already exists in "%s"', $key, $this->base));
		}
		$destination = $this->path($newName ?? $key);
		if (copy($source, $destination)) return new SplFileInfo($destination);
		throw new IOException(sprintf('Cannot copy file from %s to %s!', $source, $destination));
	}

	/**
	 * @param string $key
	 *
	 * @return boolean
	 */
	public function isFile(string $key): bool {
		return is_file($this->path($key));
	}

	/**
	 * Get the absolute path for the given key.
	 *
	 * @param string|null $key
	 *
	 * @return string
	 */
	public function path(string $key = null): string {
		return $this->base . ($key !== null ? DIRECTORY_SEPARATOR . basename($key) : '');
	}

	public function importDeep(SplFileInfo $media, string $path) {
		$dest = $this->base . DIRECTORY_SEPARATOR . $path;
		if (is_file($dest)) return;
		if (!is_dir(dirname($dest))) mkdir(dirname($dest), 0777, true);
		copy($media->getRealPath(), $dest);
	}

	/**
	 * @return string
	 */
	public function name(): string {
		return basename($this->base);
	}

	/**
	 * Get the contents of all files in this filesystem as array. The filename will be used as key.
	 *
	 * @param array $extensions A list of extensions to filter from the result
	 * @param bool $removeExtension When true, the extension is removed from the array key
	 *
	 * @return array
	 */
	public function readAll(array $extensions = [], bool $removeExtension = false): array {
		$extensions = array_map(function ($extension) {
			return mb_strtolower($extension);
		}, $extensions);
		$files = [];
		$iterator = new DirectoryIterator($this->base);
		foreach ($iterator as $fileInfo) {
			if (!$fileInfo->isDot() && $fileInfo->isFile()) {
				$extension = mb_strtolower($fileInfo->getExtension());
				if (empty($extensions) || in_array($extension, $extensions)) {
					$suffix = $removeExtension ? '.' . $extension : '';
					$files[$fileInfo->getBasename($suffix)] = $this->read($fileInfo->getBasename());
				}
			}
		}
		return $files;
	}

	/**
	 * Get the contents of the file or null if the file does not exist
	 *
	 * @param string $key
	 *
	 * @return mixed
	 */
	public function read(string $key): ?string {
		if (!$this->isFile($key)) {
			return null;
		}
		return file_get_contents($this->path($key));
	}

	public function readAllRecursive(array $extensions = [], bool $removeExtension = false): array {
		$extensions = array_map(function ($extension) {
			return mb_strtolower($extension);
		}, $extensions);
		$files = [];
		/** @var RecursiveDirectoryIterator $iterator */
		$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->base, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::SELF_FIRST);
		/** @var SplFileInfo $fileInfo */
		foreach ($iterator as $fileInfo) {
			if ($fileInfo->isFile()) {
				$extension = mb_strtolower($fileInfo->getExtension());
				if (empty($extensions) || in_array($extension, $extensions)) {
					$suffix = $removeExtension ? '.' . $extension : '';
					$files[str_replace('.json', '', str_replace('\\', '/', $iterator->getSubPathName()))] = file_get_contents($fileInfo->getRealPath());
				}
			}
		}
		return $files;
	}

	/**
	 * @param bool $includeChildren
	 *
	 * @return int
	 * @throws IOException
	 */
	public function size(bool $includeChildren = false): int {
		$size = 0;
		foreach ($this->all() as $fileInfo) {
			$size += $fileInfo->getSize();
			if ($includeChildren && $fileInfo->isDir()) {
				$size += $this->down($fileInfo->getRealPath())->size($includeChildren);
			}
		}
		return $size;
	}

	/**
	 * @return array|SplFileInfo[]
	 */
	public function all(): array {
		$files = [];
		$iterator = new DirectoryIterator($this->base);
		foreach ($iterator as $fileInfo) {
			if (!$fileInfo->isDot()) {
				$files[] = new SplFileInfo($fileInfo->getRealPath());
			}
		}
		return $files;
	}

	/**
	 * Ascend to the parent directory. Returns a new filesystem.
	 *
	 * @return FileSystem
	 * @throws IOException
	 */
	public function up(): FileSystem {
		return new static(dirname($this->base));
	}

	/**
	 * Write the given content to the file
	 *
	 * @param string $key
	 * @param string $data
	 *
	 * @return bool
	 */
	public function write(string $key, string $data): bool {
		return file_put_contents($this->path($key), $data, LOCK_EX) !== false;
	}

	/**
	 * @throws IOException
	 */
	public static function fromFile(SplFileInfo $file): FileSystem {
		return new static($file->getRealPath());
	}

	public static function parent(SplFileInfo $file): SplFileInfo {
		return new SplFileInfo(dirname($file->getRealPath()));
	}
}